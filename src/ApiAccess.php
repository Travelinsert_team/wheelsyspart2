<?php
/**
 * class api access
 * defines functions that affect on WheelsServices
 *
 * PHP verion  7.1.3
 *
 * @category   Class
 * @author     raman <raman@travelinsert.com>
 *
 * @version    0.1
 */
namespace Apiaccess;

define('APIKEY', '123456789');

use Wheelsservices\WheelsServices;

/**
 * ApiAccess Class
 *
 * This class is used for send and receive the XML from Wheesys API.
 * @filesource ApiAccess.php
 * @api Wheelsys API
 * @since 1.0
 */

class ApiAccess
{
    /**
     * ValidateApi method
     *
     * to validate the apikey entered
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param $apikey is used to get the apikey
     *
     * @return json string
     **/
    public function validateApi($apikey)
    {
        if (APIKEY == $apikey) {
            return true;
        } else {
            return false;
        }		
    }
	
    /**
     * GetStation List
     *
     * This function is used to get all station list from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param array $data is used to apikey,get
     *
     * @return json string
     **/

    public function station($data)
    {
        $validate = self::validateApi($data['apikey']);
        if ($validate == true) {
            $station = WheelsServices::station();
        } else {
            $station = 'API key is not valid';
        }
        return $station;
    }
	
    /**
     * Get Price Quote List
     *
     * This function is used to get price quote list for rates and availability of the vehicles from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param array $data is used to apikey,get from,from_time,to,to_time,pickup_stationm,return_station,pickup_point,
     * dropoff_point, apikey
     *
     * @return json string
     **/

    public function price($data)
    {
        $validate = self::validateApi($data['apikey']);
        if ($validate == true) {
            $priceqoute = WheelsServices::priceQuote($data);
        } else {
            $pricequote = 'API key is not valid';
        }
      
	    return $priceqoute;
    }

    /**
     * Get Groups List
     *
     * This function is used to get the vehicle's model and group from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param $apikey is used to get the apikey
     *
     * @return json string
     **/

    public function group($data)
    {
        $validate = self::validateApi($data['apikey']);
        if ($validate == true) {
            $groups = WheelsServices::groups();
        } else {
            $groups = 'API key is not valid';
        }
        return $groups;
    }

    /**
     * Get Reservation
     *
     * This function is used to make the reservation of the vehicles from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param array $data is used to apikey,get from ,from_time,to,to_time,pickup_station,return_station,customer_name,
     * voucherno,pickup_info,return_info,customer_email,quoteref_id,pickup_point,remarks,option_code
     *
     * @return json string
     */

    public function reservation($data)
    {   
	    $apikey = $data['apikey'];
	    $from = $data['from']; //14/12/2018
        $from_time = $data['from_time']; //1200
        $to = $data['to']; //18/12/2018
        $to_time = $data['to_time']; //2200
        $pickup_station = $data['pickup_station']; //A9K
        $return_station = $data['return_station']; //A9K //optional variable
        $customer_name = $data['customer_name'];
        $group = $data['group'];
        $voucherno = $data['voucherno'];
        $pickup_info = $data['pickup_info']; //optional variable  /* If outside the office or Flight No */
        $return_info = $data['return_info']; //optional variable  /* If outside the office or any specific area */
        $customer_email = $data['customer_email']; //optional variable
        $customer_phone = $data['customer_phone']; //optional variable
        $quoteref_id = $data['quoteref_id']; //optional variable /* Your Price Quote ID */
        $pickup_point = $data['pickup_point']; //optional variable
        $dropoff_point = $data['dropoff_point']; //optional variable
        $remarks = $data['remarks']; //optional variable
        $option_code = $data['option_code'];

        $validate = self::validateApi($apikey);
        if ($validate == true) {
            $reservation = WheelsServices::reservation($data);
        } else {
            $reservation =  'API key is not valid';
        }
        return $reservation;
    }

    /**
     * Get expresscheckout details
     *
     * This function is used to get check to provide advance customer details such as license number & identification
     * to speed up car delivery.
     * Express information can be set even if the reservation is at on-request statusf from Wheelsys API
     *
     * @api Wheelsys API
     * @filesource ApiAccess.php
     * @param array $data is used to get $apikey,refno,irn
     *
     * @return json sting
     **/

    public function expressCheckout($data)
    {
    	$apikey = $data['apikey'];
		$refno = $data['refno']; 
    

        $validate = self::validateApi($apikey);
        if ($validate == true) {
            $expressCheckout = WheelsServices::expressCheckout($data);
        } else {
            $expressCheckout =  'API key is not valid';
        }
        return $expressCheckout;
    }
    /**
     * Get Amend Reservation  details
     *
     * This function is used to make the amendment for the reservation of the vehicles from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     * @param array $data is used to get apikey,from ,from_time,to,to_time,pickup_station,return_station,customer_name,
     * voucherno,irn,refno,customer_name,pickup_info,return_info,customer_email,customer_phone,quoteref_id,pickup_point,
     *
     * @return json string
     **/

    public function amendReservation($data)
    {
    	$apikey = $data['apikey'];
        $from = $data['from']; //14/12/2018
        $from_time = $data['from_time']; //1200
        $to = $data['to']; //18/12/2018
        $to_time = $data['to_time']; //2200
        $pickup_station = $data['pickup_station']; //A9K
        $return_station = $data['return_station']; //A9K
        $customer_name = $data['customer_name'];
        $group = $data['group'];
        $voucherno = $data['voucherno'];
        $irn =  $data['irn'];
        $refno = $data['refno'];
        $pickup_info = $data['pickup_info']; //optional variable  /* If outside the office or Flight No */
        $return_info = $data['return_info']; //optional variable  /* If outside the office or any specific area */
        $customer_email = $data['customer_email']; //optional variable
        $customer_phone = $data['customer_phone']; //optional variable
        $quoteref_id = $data['quoteref_id']; //optional variable /* Your Price Quote ID */
        $pickup_point = $data['pickup_point']; //optional variable
        $dropoff_point = $data['dropoff_point']; //optional variable
        $remarks = $data['remarks']; //optional variable
    

        $validate = self::validateApi($apikey);
        if ($validate == true) {
            $amend = WheelsServices::amendReservation($data);
        } else {
            $amend =  'API key is not valid';
        }
        return $amend;
    }

    /**
     * Get cancel Reservation  details
     *
     * This function is used to cancel reservation of the vehicles from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param array $data is used to get apikey,refno,irn
     *
     * @return json string
     **/

    public function cancelReservation($data)
    {
		$apikey = $data['apikey'];
        $irn =  $data['irn'];
        $validate = self::validateApi($apikey);
        if ($validate == true) {
            $cancellation = WheelsServices::cancelReservation($data);
        } else {
            $cancellation = 'API key is not valid';
        }
        return $cancellation;
    }

    /**
     * Get Reservation details
     *
     * This function is used to get reservation  details of reserved vehicle from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param array $data is used to get apikey,refno,irn
     * @return json string
     **/

    public function readReservation($data)
    {   
		$apikey = $data['apikey'];
        $irn =  $data['irn'];
        $validate = self::validateApi($apikey);
        if ($validate == true) {
            $readReservation = WheelsServices::readReservation($data);
        } else {
            $readReservation = 'API key is not valid';
        }
        return $readReservation;
    }

    /**
     * Get vehicle options
     *
     * This function is used to get all optiona which the vehicle has from the Wheelsys API
     * @api Wheelsys API
     * @filesource ApiAccess.php
     *
     * @param $apikey is used to get the apikey
     *
     * @return void
     **/

    public function option($data)
    {
       	$apikey = $data['apikey'];
        $validate = self::validateApi($apikey);
        if (APIKEY == $apikey) {
            $options = WheelsServices::option();
        } else {
            $options = 'API key is not valid';
        }
        return $options;
    }
}
